package com.meridian.voiceofislam.videofield;

import android.app.AlertDialog;
import android.app.ProgressDialog;
import android.content.DialogInterface;
import android.os.AsyncTask;
import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.support.v4.view.MenuItemCompat;
import android.support.v7.widget.GridLayoutManager;
import android.support.v7.widget.PopupMenu;
import android.support.v7.widget.RecyclerView;
import android.support.v7.widget.SearchView;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.Menu;
import android.view.MenuInflater;
import android.view.MenuItem;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ArrayAdapter;
import android.widget.ExpandableListAdapter;
import android.widget.ExpandableListView;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.ProgressBar;
import android.widget.Spinner;
import android.widget.TextView;
import android.widget.Toast;

import com.android.volley.Request;
import com.android.volley.RequestQueue;
import com.android.volley.Response;
import com.android.volley.VolleyError;
import com.android.volley.toolbox.StringRequest;
import com.android.volley.toolbox.Volley;
import com.arlib.floatingsearchview.FloatingSearchView;
import com.meridian.voiceofislam.R;

import org.apache.http.HttpEntity;
import org.apache.http.HttpResponse;
import org.apache.http.client.HttpClient;
import org.apache.http.client.methods.HttpPost;
import org.apache.http.impl.client.DefaultHttpClient;
import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.io.BufferedReader;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;



/**
 * Created by iFocus on 27-10-2015.
 */
public class Vism_TabTwoFragment extends Fragment implements SearchView.OnQueryTextListener {
    ArrayList<String> listOfSomething1 = new ArrayList<String>();
    String name;
    TextView tm;
    ProgressBar progress;
    String result;
//List<PartcipatedModel> parteve;
List<String> s=new ArrayList<>();
  //  PartcipatedModel pm;
    String Title_of_rg, Cme_pnts, Venues, Dates;

    int i=0;
    private RecyclerView recyclerview;
   List<CountryModel1> mCountryModel1;
    List<CountryModel2> mCountryModel2;
    CountryModel2 cm2;
RVAdapter1 adapter1;
    CountryModel1 cm;
    String id,cat_id,sub_cat_id,video,thumnail,title;
    private FloatingSearchView mSearchView,mSearchView1;

 //   JSONObject mJsonObject;

    JSONArray mArray1;
    JSONObject mJsonObject1;
    LinearLayout lx;
 //   ArrayList<String> spinnerArray;
    String response;
    private static final String REGISTER_URL = "http://app.knm.com.php56-9.dfw3-2.websitetestlink.com/services/response.php";
    ArrayList<VideoSubModel> vcat;

    ArrayList<String> spinnerArray,spinnerArray1;
    Spinner spinner;
    ArrayAdapter<String> spinnerArrayAdapter;
    Spinner spinner0, spinner1, spinner2;
    String dctor, dptmt, dvmt,dptmt1;
    String dvn_id,dvn_id1;
    ArrayList<String> catlist;
    //ArrayList<VideoCatModel> catmod;
 //
    //   VideoCatModel v_ctm;
  String cat_id3,sub_cat_id3,video3,thumbnail3,title3;
    String vcat_id,vsub_cat,vsub_cat_id;
    VideoSubModel vcm;
    private Boolean spinnerTouched = false;
   // ArrayList<VideoCatModel> catmod;
    List<VideoCatModel> catmod;
    VideoCatModel v_ctm;
    JSONArray mArray;
    JSONObject mJsonObject;
    TextView tit;
    RecyclerAdapterS adaptersp;
    ImageView im;


    String  vfilt_id,
    vfilt_nam,
    vfilt_subcat_id ,
    vfilt_cat_id;
    ArrayList<VideoFilModel> vfil;
    VideoFilModel vfm;
    ArrayList<String> fillist,fillist1,fillist2,fillist3;
    static PopupMenu popupMenu = null,popupMenu1=null;
    ImageView options;
    String k;





    ExpandableListView expandableListView;
    ExpandableListAdapter expandableListAdapter;
    List<String> expandableListTitle;
    HashMap<String, List<String>> expandableListDetail;


    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {

        View view = inflater.inflate(R.layout.vism_tab_two_fragment, container, false);
        progress = (ProgressBar)view. findViewById(R.id.progress_bar);
        //popupMenu = new PopupMenu(getActivity(),options);
        options= (ImageView) view.findViewById(R.id.img_menu);
        ExpandableListDataPump();;
        if (DetectConnection
                .checkInternetConnection(getActivity())) {
            //Toast.makeText(getActivity(),
            //	"You have Internet Connection", Toast.LENGTH_LONG)

            new DownloadData().execute();
        } else {
            AlertDialog alertDialog = new AlertDialog.Builder(getActivity()).create();
            alertDialog.setTitle("Sorry");
            alertDialog.setMessage("No network");
            alertDialog.setButton(AlertDialog.BUTTON_NEUTRAL,getResources().getString(R.string.ok),
                    new DialogInterface.OnClickListener() {
                        public void onClick(DialogInterface dialog, int which) {


                            dialog.dismiss();

                        }
                    });
            alertDialog.show();


            //	Toast.makeText(getActivity(),
            //	getResources().getString(R.string.Sorry) +
            //	getResources().getString(R.string.cic),
            //	Toast.LENGTH_SHORT).show();
        }

        // Create a grid layout with two columns
        GridLayoutManager layoutManager = new GridLayoutManager(getActivity(), 2);


        layoutManager.setSpanSizeLookup(new GridLayoutManager.SpanSizeLookup() {
            @Override
            public int getSpanSize(int position) {
                return position == 0 ? 2 : 1;
            }
        });
        recyclerview = (RecyclerView) view.findViewById(R.id.recyclerview);
        im=(ImageView) view.findViewById(R.id.imageView7);
        tit = (TextView) view.findViewById(R.id.textView3);

       // ExpandList = (ExpandableListView)view. findViewById(R.id.expandableListView);



        expandableListView = (ExpandableListView)view. findViewById(R.id.expandableListView);
      //  expandableListDetail = ExpandableListDataPump.getData();
    //    expandableListTitle = new ArrayList<String>(expandableListDetail.keySet());
        expandableListAdapter = new CustomExpandableListAdapter(getActivity(), expandableListTitle, expandableListDetail);
        expandableListView.setAdapter(expandableListAdapter);
        expandableListView.setOnGroupExpandListener(new ExpandableListView.OnGroupExpandListener() {

            @Override
            public void onGroupExpand(int groupPosition) {
             /*   Toast.makeText(getActivity(),
                        expandableListTitle.get(groupPosition) + " List Expanded.",
                        Toast.LENGTH_SHORT).show();*/
            }
        });

        expandableListView.setOnGroupCollapseListener(new ExpandableListView.OnGroupCollapseListener() {

            @Override
            public void onGroupCollapse(int groupPosition) {
              /*  Toast.makeText(getActivity(),
                        expandableListTitle.get(groupPosition) + " List Collapsed.",
                        Toast.LENGTH_SHORT).show();*/

            }
        });

        expandableListView.setOnChildClickListener(new ExpandableListView.OnChildClickListener() {
            @Override
            public boolean onChildClick(ExpandableListView parent, View v,
                                        int groupPosition, int childPosition, long id) {
             /*   Toast.makeText(
                        getActivity(),
                        expandableListTitle.get(groupPosition)
                                + " -> "
                                + expandableListDetail.get(
                                expandableListTitle.get(groupPosition)).get(
                                childPosition), Toast.LENGTH_SHORT
                ).show();*/
                expandableListView.setVisibility(View.GONE);
                //vcm.
               k= mCountryModel1.get(groupPosition).getSub_cat_id();


                System.out.println("<<%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%>>>>" +k);
                System.out.println("<<%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%>>>>" +mCountryModel1.get(groupPosition).getTitle());
                registerUser3();
                mSearchView.setVisibility(View.GONE);
                tit.setVisibility(View.VISIBLE);
                tit.setText(mCountryModel1.get(groupPosition).getTitle());
                return false;
            }
        });


        mSearchView = (FloatingSearchView) view.findViewById(R.id.floating_search_view);
      //  mSearchView1 = (FloatingSearchView) view.findViewById(R.id.floating_search_view1);

       // LinearLayoutManager layoutManager = new LinearLayoutManager(getActivity());
        recyclerview.setLayoutManager(layoutManager);
        mSearchView.setOnQueryChangeListener(new FloatingSearchView.OnQueryChangeListener() {
            @Override
            public void onSearchTextChanged(String oldQuery, final String newQuery) {
                final List<CountryModel1> filteredModelList = filter(mCountryModel1, newQuery);
                adapter1.setFilter(filteredModelList);
                //  return true

            }
        });
        im.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
             //   registerUser1();
                new DownloadData().execute();
                mSearchView.setVisibility(View.VISIBLE);
                im.setVisibility(View.GONE);
                tit.setVisibility(View.GONE);
            }
        });

        options.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                i++;
                if (i == 1) {
                    //Single click
                    expandableListView.setVisibility(View.VISIBLE);
                } else if (i == 2) {
                    //Double click

                    expandableListView.setVisibility(View.GONE);
                    i = 0;
                    //   Toast.makeText(getActivity(),"clicked",Toast.LENGTH_LONG).show();
                }
            }
        });

     /*   options.setOnTouchListener(new View.OnTouchListener() {
            @Override
            public boolean onTouch(View view, MotionEvent motionEvent) {
                expandableListView.setVisibility(View.VISIBLE);
                mSearchView.setVisibility(View.VISIBLE);
                im.setVisibility(View.GONE);
                tit.setVisibility(View.GONE);
              //  popupMenu.show();

//                System.out.println("<<<........................tttttttttttt......................................>>>>" +vsub_cat.toString());
              //  System.out.println("<<<...................vvvvvvvvvvvvvvvvvvvvvvvvvv...........................................>>>>" +spinnerArray1);
                return false;
            }
        });*/
        //  new DownloadData().execute();

return  view;
    }




    private void registerUser3() {
        progress.setVisibility(View.VISIBLE);

        StringRequest stringRequest = new StringRequest(Request.Method.POST, REGISTER_URL,
                new Response.Listener<String>() {
                    @Override
                    public void onResponse(String response) {
                        // Toast.makeText(getActivity(), response, Toast.LENGTH_LONG).show();
                        catmod = new ArrayList<VideoCatModel>();
                        mCountryModel2=new ArrayList<>();
                        mCountryModel1 = new ArrayList<>();
                        mCountryModel1.clear();
                        try {
                            mArray = new JSONArray(response);
                            for (int i = 0; i < mArray.length(); i++) {
                                v_ctm = new VideoCatModel();
                                cm=new CountryModel1();

                                mJsonObject = mArray.getJSONObject(i);
                                Log.d("OutPut", mJsonObject.getString("category_id"));
                                Log.d("OutPut2", mJsonObject.getString("sub_category_id"));
                                Log.d("OutPut3", mJsonObject.getString("video"));
                                Log.d("OutPut2", mJsonObject.getString("thumbnail"));
                                Log.d("OutPut3", mJsonObject.getString("title"));


                                cat_id3 = mJsonObject.getString("category_id");
                                sub_cat_id3 = mJsonObject.getString("sub_category_id");
                                video3 = mJsonObject.getString("video");
                                thumbnail3 = mJsonObject.getString("thumbnail");
                                title3 = mJsonObject.getString("title");
                               v_ctm.setCat_id(cat_id3);
                               v_ctm.setSub_cat_id(sub_cat_id3);
                              v_ctm.setVideo(video3);
                             //  v_ctm.setVideo(video3);
                               v_ctm.setThumbnail(thumbnail3);
                               v_ctm.setTitle(title3);
                                catmod.add(v_ctm);

                                System.out.println(""+catmod);




                                System.out.println("<<<33333333333333cat_id>>>>" +cat_id3);
                                System.out.println("<<<3333333333333sub_cat_id>>>" +sub_cat_id3);
                                System.out.println("<<<3333333333333video>>>" +video3);
                                System.out.println("<<<333333333333thumbnail>>>>" +thumbnail3);
                                System.out.println("<<<33333333333333333title>>>>" +title3);
                                recyclerview.setHasFixedSize(true);
                                //to use RecycleView, you need a layout manager. default is LinearLayoutManager
                            //    LinearLayoutManager linearLayoutManager=new LinearLayoutManager(getActivity());
                               // linearLayoutManager.setOrientation(LinearLayoutManager.VERTICAL);



                                GridLayoutManager layoutManager = new GridLayoutManager(getActivity(), 2);

                                // Create a custom SpanSizeLookup where the first item spans both columns
                                layoutManager.setSpanSizeLookup(new GridLayoutManager.SpanSizeLookup() {
                                    @Override
                                    public int getSpanSize(int position) {
                                        return position == 0 ? 2 : 1;
                                    }
                                });

                                recyclerview.setLayoutManager(layoutManager);
                               /* RecyclerAdapterSp adapter2=new RecyclerAdapterSp(catmod,getActivity());
                                recyclerview.setAdapter(adapter2);*/

                                 adaptersp = new RecyclerAdapterS(catmod,getActivity());
                                recyclerview.setAdapter(adaptersp);
                                mCountryModel1.add(cm);
                          mSearchView.setVisibility(View.GONE);
                                im.setVisibility(View.VISIBLE);
                                tit.setVisibility(View.VISIBLE);
                               // tit.setText(dptmt);
                           //
                                // 0  System.out.println(""+dptmt);

                                /*adapter1 = new RVAdapter1(mCountryModel1,getActivity());
                                recyclerview.setAdapter(adapter1);*/
                                progress.setVisibility(View.GONE);
                            }
                        } catch (JSONException e) {
                            e.printStackTrace();
                        }

                    }
                },
                new Response.ErrorListener() {
                    @Override
                    public void onErrorResponse(VolleyError error) {
                        Toast.makeText(getActivity(), error.toString(), Toast.LENGTH_LONG).show();
                    }
                }) {
            @Override
            protected Map<String, String> getParams() {
                Map<String, String> params = new HashMap<String, String>();
                params.put("fid", "3");
                params.put("category_id","2");
                params.put("subcategory_id",k);
                return params;
            }

        };

        RequestQueue requestQueue = Volley.newRequestQueue(getActivity());
        requestQueue.add(stringRequest);
    }








    @Override
    public void onViewCreated(View view, Bundle savedInstanceState) {
        super.onViewCreated(view, savedInstanceState);

        setHasOptionsMenu(true);



    }


    @Override
    public void onCreateOptionsMenu(Menu menu, MenuInflater inflater) {
        inflater.inflate(R.menu.menu_main, menu);

        final MenuItem item = menu.findItem(R.id.action_search);
        final SearchView searchView = (SearchView) MenuItemCompat.getActionView(item);
        searchView.setVisibility(View.GONE);

    }

    @Override
    public boolean onQueryTextChange(String newText) {
        final List<CountryModel1> filteredModelList = filter(mCountryModel1, newText);
        adapter1.setFilter(filteredModelList);
        return true;
    }

    @Override
    public boolean onQueryTextSubmit(String query) {
        return false;
    }


    private List<CountryModel1> filter(List<CountryModel1> models, String query) {
        query = query.toLowerCase();

        final List<CountryModel1> filteredModelList = new ArrayList<>();
        for (CountryModel1 model1 : models) {
            final String text = model1.getTitle().toLowerCase();
            if (text.contains(query)) {
                filteredModelList.add(model1);
            }
        }
        return filteredModelList;
    }






    class DownloadData extends AsyncTask<Void, Void, Void> {

            ProgressDialog pd = null;


            @Override
            protected void onPreExecute() {
                super.onPreExecute();
                progress.setVisibility(ProgressBar.VISIBLE);

//

            }

            @Override
            protected Void doInBackground(Void... params) {
//http://zulekhahospitals.com/json-cme.php?fid=103&txtemail=rajeesh@meridian.net.in&password=123456&user_type=Synapse%20User

                try {



                    HttpClient httpclient = new DefaultHttpClient();


                    HttpPost httppost = new HttpPost("http://app.knm.com.php56-9.dfw3-2.websitetestlink.com/services/response.php?fid=3&category_id=2"
                    );
                    HttpResponse response = httpclient.execute(httppost);
                    HttpEntity entity = response.getEntity();
                    InputStream is = entity.getContent();
                    BufferedReader reader = new BufferedReader(new InputStreamReader(is, "iso-8859-1"), 8);
                    StringBuilder sb = new StringBuilder();
                    String line = null;
                    while ((line = reader.readLine()) != null) {
                        sb.append(line + "\n");
                    }
                    is.close();
                    result = sb.toString();


                    // Toast.makeText(getApplicationContext(), ""+result, Toast.LENGTH_SHORT).show();
                } catch (Exception e) {
                    Log.e("Loading connection  :", e.toString());
                }


                return null;
            }

            protected void onPostExecute(Void args) {

                progress.setVisibility(ProgressBar.GONE);
                String sam = result.trim();
                System.out.println(">>>>>>>>>>>>>>>" + result);
                System.out.println(">>>>>>>>>>>>>>>" + sam);
                String value = result;
                if
                        (result.equalsIgnoreCase("[]")) {
                    Toast.makeText(getActivity(), "No Events", Toast.LENGTH_SHORT).show();
                } else {
                    JSONArray mArray;
                    mCountryModel1 = new ArrayList<>();
                    mCountryModel1.clear();
                    //  parteve = new ArrayList<PartcipatedModel>();
                    try {
                        mArray = new JSONArray(result);
                        for (int i = 0; i < mArray.length(); i++) {

                           // pm = new PartcipatedModel();
                            cm=new CountryModel1();

                        //   parteve = new ArrayList<PartcipatedModel>();
                            //  em= new ExampleModel();
                            JSONObject mJsonObject = mArray.getJSONObject(i);
                            Log.d("OutPut", mJsonObject.getString("id"));
                        Log.d("OutPut2", mJsonObject.getString("category_id"));
                        Log.d("OutPut3", mJsonObject.getString("sub_category_id"));

                        Log.d("OutPut2", mJsonObject.getString("video"));
                             Log.d("OutPut3", mJsonObject.getString("thumbnail"));

                            Log.d("OutPut4", mJsonObject.getString("title"));
                           // Log.d("OutPut5", mJsonObject.getString("id"));


                            id = mJsonObject.getString("id");
                            cat_id= mJsonObject.getString("category_id");
                            sub_cat_id= mJsonObject.getString("sub_category_id");
                            video= mJsonObject.getString("video");
                            thumnail= mJsonObject.getString("thumbnail");
                            title= mJsonObject.getString("title");
                            cm.setId(id);
                            cm.setCat_id(cat_id);
                            cm.setSub_cat_id(sub_cat_id);
                            cm.setVideo(video);
                            cm.setThumnail(thumnail);
                            cm.setTitle(title);

                            System.out.println("<<<kk>>>>" + Title_of_rg);
//

                            mCountryModel1.add(cm);

                            System.out.println(""+mCountryModel1);

                            adapter1 = new RVAdapter1(mCountryModel1,getActivity());
                            recyclerview.setAdapter(adapter1);


                        }
                    } catch (JSONException e) {
                        e.printStackTrace();
                    }

//
                }
//

            }

//

        }
    public void ExpandableListDataPump() {

        expandableListTitle = new ArrayList<String>();


        expandableListTitle.add("Islamic Videos");
        expandableListTitle.add("Documentaries");
        expandableListTitle.add("Thoughts");



          expandableListDetail = new HashMap<String, List<String>>();

            List<String> islamic_videos = new ArrayList<String>();
            islamic_videos.add(" Talks, seminars and discussions");
            islamic_videos.add("Animated videos for children");
            islamic_videos.add("Visualized Fik’hu – Learn Namaz, vudhu etc");
            islamic_videos.add("sharing short videos providing good messages from various organizations worldwide");


            List<String> documentaries = new ArrayList<String>();
            documentaries.add("Short Islamic Documentaries");

            List<String> thoughts = new ArrayList<String>();
            thoughts.add("Thought for the day");
            thoughts.add("Short Video clips");
            thoughts.add("Organizational response in recent issues video");
            expandableListDetail.put(expandableListTitle.get(0), islamic_videos);
            expandableListDetail.put(expandableListTitle.get(1), documentaries);
            expandableListDetail.put(expandableListTitle.get(2), thoughts);





    }


}
