package com.meridian.voiceofislam;


import android.Manifest;
import android.content.Context;
import android.content.Intent;
import android.content.pm.PackageManager;
import android.net.Uri;
import android.os.Bundle;
import android.support.design.widget.NavigationView;
import android.support.design.widget.TabLayout;
import android.support.v4.app.ActivityCompat;
import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentPagerAdapter;
import android.support.v4.view.GravityCompat;
import android.support.v4.view.ViewPager;
import android.support.v4.widget.DrawerLayout;
import android.support.v7.app.ActionBarDrawerToggle;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.Toolbar;
import android.view.Menu;
import android.view.MenuItem;
import android.widget.Toast;

import com.meridian.voiceofislam.Radio.AudioVisualizationFragment;
import com.meridian.voiceofislam.Radio.BlankFragment;
import com.meridian.voiceofislam.audioplayer.AudioFragment;
import com.meridian.voiceofislam.livestream.LiveStreamFragment;
import com.meridian.voiceofislam.sidebar.AboutUs;
import com.meridian.voiceofislam.sidebar.Contact;
import com.meridian.voiceofislam.sidebar.ListMp3;
import com.meridian.voiceofislam.videoaudioupload.Loginfragment;
import com.meridian.voiceofislam.videofield.Vism_TabTwoFragment;

import uk.co.chrisjenx.calligraphy.CalligraphyContextWrapper;

public class MainActivity extends AppCompatActivity
        implements NavigationView.OnNavigationItemSelectedListener,AudioFragment.OnFragmentInteractionListener,LiveStreamFragment.OnFragmentInteractionListener,BlankFragment.OnFragmentInteractionListener {

    ViewPager viewPager;

    TabLayout tabLayout;
    static int tabno;
    private static final int REQUEST_CODE = 1;

BlankFragment blnk;
    String  REGISTER_URL  ="http://app.knm.com.php56-9.dfw3-2.websitetestlink.com/services/response.php";

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);
        Toolbar toolbar = (Toolbar) findViewById(R.id.toolbar);
        setSupportActionBar(toolbar);
        if(savedInstanceState==null)

        {
            if (ActivityCompat.checkSelfPermission(MainActivity.this, Manifest.permission.RECORD_AUDIO) == PackageManager.PERMISSION_DENIED && ActivityCompat.checkSelfPermission(MainActivity.this, Manifest.permission.WRITE_EXTERNAL_STORAGE) == PackageManager.PERMISSION_DENIED && ActivityCompat.checkSelfPermission(MainActivity.this, Manifest.permission.READ_EXTERNAL_STORAGE) == PackageManager.PERMISSION_DENIED) {

                requestPermissions();

            }

        }



        viewPager=(ViewPager)findViewById(R.id.viewpager);
        tabLayout=(TabLayout)findViewById(R.id.tabs);


        DrawerLayout drawer = (DrawerLayout) findViewById(R.id.drawer_layout);
        ActionBarDrawerToggle toggle = new ActionBarDrawerToggle(
                this, drawer, toolbar, R.string.navigation_drawer_open, R.string.navigation_drawer_close);
        drawer.setDrawerListener(toggle);
        toggle.syncState();

        NavigationView navigationView = (NavigationView) findViewById(R.id.nav_view);
        navigationView.setNavigationItemSelectedListener(this);


//        NetworkCheckingClass networkCheckingClass = new NetworkCheckingClass(getApplicationContext());
//        boolean i = networkCheckingClass.ckeckinternet();
//        if (i)
//        {
//
////          progress.setVisibility(ProgressBar.VISIBLE);
//            StringRequest stringRequest = new StringRequest(Request.Method.POST, REGISTER_URL,
//                    new Response.Listener<String>() {
//                        @Override
//                        public void onResponse(String response) {
//                            //  progress.setVisibility(ProgressBar.GONE);
//                            System.out.println("responseeeee"+response);
//                            JSONArray jsonArray=new JSONArray();
//                           categoryModelArrayList=new ArrayList<>();
//
//                            try {
//                                for(int i=0;i< jsonArray.length();i++) {
//                                    CategoryModel categoryModel=new CategoryModel();
//                                    JSONObject jsonobject =jsonArray.getJSONObject(i);
//                                    String category_id = jsonobject.getString("category_id");
//                                    String category = jsonobject.getString("category");
//                                    categoryModel.setCategory_id(category_id);
//                                    categoryModel.setCategory(category);
//                                    categoryModelArrayList.add(categoryModel);
//
//
//                                }
//
//
//
//                            } catch (JSONException e) {
//                                e.printStackTrace();
//                            }
//                        }
//                    },
//                    new Response.ErrorListener() {
//                        @Override
//                        public void onErrorResponse(VolleyError error) {
//
//                        }
//                    }) {
//                @Override
//                protected Map<String, String> getParams() throws AuthFailureError {
//                    Map<String, String> params = new HashMap<String, String>();
//
//
//
//                    params.put("fid", "1");
//
//
//                    return params;
//                }
//
//            };
//
//            RequestQueue requestQueue = Volley.newRequestQueue(this);
////            int socketTimeout = 30000;//30 seconds - change to what you want
////            RetryPolicy policy = new DefaultRetryPolicy(socketTimeout, DefaultRetryPolicy.DEFAULT_MAX_RETRIES, DefaultRetryPolicy.DEFAULT_BACKOFF_MULT);
////            stringRequest.setRetryPolicy(policy);
//            requestQueue.add(stringRequest);
//        }
//

//        AudioFragment vidgallfrag=new AudioFragment();
//        //vidgallfrag.setArguments(bundle);
//        FragmentManager fragmentManager = getSupportFragmentManager();
//
//        fragmentManager.beginTransaction().replace(R.id.mainfrag,vidgallfrag).commit();

        viewPager.setAdapter(new FragmentPagerAdapter(getSupportFragmentManager()) {
            @Override
            public Fragment getItem(int position) {
                tabno = position;


                switch (position){


                    case 0: //if (Build.VERSION.SDK_INT >= 23)
                    {//ActivityCompat.requestPermissions(MainActivity.this, new String[]{Manifest.permission.WRITE_EXTERNAL_STORAGE}, 1);
                        //do your check here
                        AudioFragment tabf = new AudioFragment();
                        return tabf;
                    }







                    case 1:

//                        TabThreeFragment tbtf=new TabThreeFragment();
//                        return tbtf;

                        Vism_TabTwoFragment vnf=new Vism_TabTwoFragment();
                        return vnf;



                    case 2:

                       //   requestPermissions();
                            AudioVisualizationFragment blnk = new AudioVisualizationFragment();
                            return blnk;






                    case 3:LiveStreamFragment lsf=new LiveStreamFragment();
                        return lsf;



                    default: BlankFragment tabf1 = new BlankFragment();
                        return tabf1;



                }






            }

            @Override
            public CharSequence getPageTitle(int position)
            { //String category=categoryModelArrayList.get(position).getCategory();

                if(position==0) {
                 //  String category= categoryModelArrayList.get(0).getCategory();
                    return "Audio";
                }

                if(position==1) {
                  //  String category=   categoryModelArrayList.get(1).getCategory();
                    return "Video";
                }



                if(position==2) {
                  //  String category= categoryModelArrayList.get(2).getCategory();
                    return "Live Audio";
                }

                if(position==3) {
                   // String category= categoryModelArrayList.get(3).getCategory();
                    return "Live Video";
                }



//                if(position==4) {
//
//                    return "Quran Recitations";
//
//                }


                   else {
                    return  null;




                }



            }

            @Override
            public int getCount() {
                return 4;
            }
        });

        //indique au tablayout quel est le viewpager à écouter
        tabLayout.setupWithViewPager(viewPager);

    }

    @Override
    public void onBackPressed() {
        DrawerLayout drawer = (DrawerLayout) findViewById(R.id.drawer_layout);
        if (drawer.isDrawerOpen(GravityCompat.START)) {
            drawer.closeDrawer(GravityCompat.START);
        } else {
            super.onBackPressed();
        }
    }

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        // Inflate the menu; this adds items to the action bar if it is present.
        //getMenuInflater().inflate(R.menu.main, menu);
        return true;
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        // Handle action bar item clicks here. The action bar will
        // automatically handle clicks on the Home/Up button, so long
        // as you specify a parent activity in AndroidManifest.xml.
        int id = item.getItemId();

        //noinspection SimplifiableIfStatement
//        if (id == R.id.action_settings) {
//            return true;
//        }

        if (id == R.id.menuSortOrator) {

            Toast.makeText(getApplicationContext(),"Sorting by Orator",Toast.LENGTH_SHORT).show();
            return true;
        }

        if (id == R.id.menuSortSubject) {
            Toast.makeText(getApplicationContext(),"Sorting by Subject",Toast.LENGTH_SHORT).show();
            return true;
        }

        if (id == R.id.menuSortProgram) {
            Toast.makeText(getApplicationContext(),"Sorting by Program",Toast.LENGTH_SHORT).show();
            return true;
        }


        return super.onOptionsItemSelected(item);
    }

    @SuppressWarnings("StatementWithEmptyBody")
    @Override
    public boolean onNavigationItemSelected(MenuItem item) {
        // Handle navigation view item clicks here.
        int id = item.getItemId();

        if (id == R.id.nav_about) {
//            AudioFragment vidgallfrag=new AudioFragment();
//       //vidgallfrag.setArguments(bundle);
//       FragmentManager fragmentManager = getSupportFragmentManager();
//        fragmentManager.beginTransaction().replace(R.id.mainfrag,vidgallfrag).commit();
            Intent s=new Intent(getApplicationContext(),AboutUs.class);
            startActivity(s);

            // Handle the camera action
        } else if (id == R.id.nav_upload) {
            Intent inte=new Intent(getApplicationContext(), Loginfragment.class);
            startActivity(inte);

        } else if (id == R.id.nav_download) {
            if (ActivityCompat.checkSelfPermission(getApplicationContext(), Manifest.permission.WRITE_EXTERNAL_STORAGE) == PackageManager.PERMISSION_GRANTED&&ActivityCompat.checkSelfPermission(getApplicationContext(), Manifest.permission.READ_EXTERNAL_STORAGE) == PackageManager.PERMISSION_GRANTED)
            {
                Intent s = new Intent(getApplicationContext(), ListMp3.class);
                startActivity(s);
            }
            else
            {
                Toast.makeText(getApplicationContext(),"Feature Not Available Due To Permission Denial",Toast.LENGTH_SHORT).show();
            }
        } else if (id == R.id.nav_contact) {
            Intent s=new Intent(getApplicationContext(),Contact.class);
            startActivity(s);
        }
//        else if (id == R.id.nav_feedback) {
//
//
//
//
//
//
//        }
        DrawerLayout drawer = (DrawerLayout)findViewById(R.id.drawer_layout);
        drawer.closeDrawer(GravityCompat.START);
        return true;
    }


    @Override
    protected void attachBaseContext(Context newBase)
    {
        super.attachBaseContext(CalligraphyContextWrapper.wrap(newBase));
    }

    @Override
    public void onFragmentInteraction(Uri uri)
    {

    }


    private void requestPermissions()
    {
        ActivityCompat.requestPermissions(
                this,
                new String[]{Manifest.permission.RECORD_AUDIO,Manifest.permission.WRITE_EXTERNAL_STORAGE,Manifest.permission.READ_EXTERNAL_STORAGE},
                REQUEST_CODE
        );
       // ActivityCompat.requestPermissions(this, new String[]{Manifest.permission.WRITE_EXTERNAL_STORAGE}, 1);
    }

    private void permissionsNotGranted()
    {
        Toast.makeText(this, R.string.toast_permissions_not_granted, Toast.LENGTH_SHORT).show();
        finish();
    }

    @Override
    public void onRequestPermissionsResult(int requestCode,
                                           String permissions[], int[] grantResults) {
        switch (requestCode) {
            case REQUEST_CODE: {
                // If request is cancelled, the result arrays are empty.
                if (grantResults.length > 0 && grantResults[0] == PackageManager.PERMISSION_GRANTED) {



                    /*Toast.makeText(this, "PERMISSION GRANTED", Toast.LENGTH_SHORT).show();*/


//                    if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.M) {
//                        shouldShowRequestPermissionRationale(Manifest.permission.RECORD_AUDIO);
//                    }


                    // permission was granted, yay! Do the
                    // contacts-related task you need to do.

                } else {

                    // permission denied, boo! Disable the
                    // functionality that depends on this permission.

                    /*Toast.makeText(this, "PERMISSION NOT GRANTED", Toast.LENGTH_SHORT).show();*/

                }
                return;
            }

            // other 'case' lines to check for other
            // permissions this app might request
        }
    }

//    @Override
//    public void onRequestPermissionsResult(int requestCode, String[] permissions, int[] grantResults) {
//        super.onRequestPermissionsResult(requestCode, permissions, grantResults);
//        if(grantResults[0]== PackageManager.PERMISSION_GRANTED)
//        {
//           // Log.v("Permission:"+permissions[0]+ "was "+grantResults[0]);
//            //resume tasks needing this permission
//        }
//    }

}
