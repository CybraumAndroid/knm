package com.meridian.voiceofislam.audioplayer;

import android.Manifest;
import android.content.Context;
import android.content.Intent;
import android.content.pm.PackageManager;
import android.content.pm.ResolveInfo;
import android.graphics.Point;
import android.media.AudioManager;
import android.media.MediaPlayer;
import android.net.Uri;
import android.os.Build;
import android.os.Bundle;
import android.os.Handler;
import android.support.annotation.NonNull;
import android.support.v4.app.ActivityCompat;
import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentTabHost;
import android.support.v4.content.ContextCompat;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.PopupMenu;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ExpandableListView;
import android.widget.ImageView;
import android.widget.PopupWindow;
import android.widget.ProgressBar;
import android.widget.RelativeLayout;
import android.widget.SeekBar;
import android.widget.TextView;
import android.widget.Toast;

import com.android.volley.AuthFailureError;
import com.android.volley.Request;
import com.android.volley.RequestQueue;
import com.android.volley.Response;
import com.android.volley.VolleyError;
import com.android.volley.toolbox.StringRequest;
import com.android.volley.toolbox.Volley;
import com.arlib.floatingsearchview.FloatingSearchView;
import com.meridian.voiceofislam.NetworkCheckingClass;
import com.meridian.voiceofislam.R;
import com.meridian.voiceofislam.RecyclerItemClickListener;
import com.sothree.slidinguppanel.SlidingUpPanelLayout;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;


/**testttttttttttttt rishikaaa///////////////////////////////////
 * A simple {@link Fragment} subclass.
 * Activities that contain this fragment must implement the
 * {@link OnFragmentInteractionListener} interface
 * to handle interaction events.
 * Use the {@link AudioFragment#newInstance} factory method to
 * create an instance of this fragment.
 */
public class AudioFragment extends Fragment implements ExpandableListView.OnClickListener {
    // TODO: Rename parameter arguments, choose names that match
    // the fragment initialization parameters, e.g. ARG_ITEM_NUMBER




    private static final String ARG_PARAM1 = "param1";
    private static final String ARG_PARAM2 = "param2";
    public static MediaPlayer mediaPlayer;
    public static ImageView implay, impause, imforward, imbackward;
    public static ImageView mImageView, downplay;
    public static SlidingUpPanelLayout mLayout;
    public static RelativeLayout cntrl, bottomplay;
    static TextView totalTime, currentTime, now_playing_audio;
    static RecyclerView rv;
    static List<AudioModel> filteredlistclick;
    static ArrayList<AudioModel> audioModelArrayList;
    static ExpandableListView expandableListView;
    static String audioFile;
    static PopupMenu popupMenu = null, popupMenu1 = null;
    String  REGISTER_URL  ="http://app.knm.com.php56-9.dfw3-2.websitetestlink.com/services/response.php";
    ArrayList<AudioFilterModel> arrayList ;
    List<String> listDataHeader;
    HashMap<String, List<String>> listDataChild;
    HashMap<String ,List<String>>listchild;
    ArrayList<AudioFilterContentModel> subCategoryModelArrayList2;
    ExpandableListAdapter expandableListAdapter;
    String audio;
    int i=0;
    String path="http://app.knm.com.php56-9.dfw3-2.websitetestlink.com/uploads/audio/";
    ProgressBar progress;
    ArrayList<String> arraylist_options,arraylist_options1,arraylist_options_sub_id, arraylist_options_filter;
    StringRequest stringRequest;
    RVAdapterAudioList rvAdapterAudioList;
    String thumbnail;
    ImageView options;
    TextView category;
      RVAdapterFilterAudioContent rvAdapterFilterAudioContent;
    RVAdapterFilterAudio rvAdapterFilterAudio;
    Point p;
    // TODO: Rename and change types of parameters
    private String mParam1;
    private String mParam2;
    private SeekBar seekBar;
    private FragmentTabHost tabHost;
    private FloatingSearchView mSearchView;
    private OnFragmentInteractionListener mListener;
    private Boolean spinnerTouched = false;
    private PopupWindow pwindo;
    // DBHelper dbHelper;
    private Handler mHandler = new Handler();
    private Runnable mRunnable = new Runnable() {

        @Override
        public void run() {
            if (mediaPlayer != null) {

                //set max value
                int mDuration = mediaPlayer.getDuration();
                seekBar.setMax(mDuration);

                //update total time text view

                totalTime.setText(getTimeString(mDuration));

                //set progress to current position
                int mCurrentPosition = mediaPlayer.getCurrentPosition();
                seekBar.setProgress(mCurrentPosition);

                //update current time text view

                currentTime.setText(getTimeString(mCurrentPosition));

                //handle drag on seekbar
                seekBar.setOnSeekBarChangeListener(new SeekBar.OnSeekBarChangeListener() {

                    @Override
                    public void onStopTrackingTouch(SeekBar seekBar) {

                    }

                    @Override
                    public void onStartTrackingTouch(SeekBar seekBar) {

                    }

                    @Override
                    public void onProgressChanged(SeekBar seekBar, int progress, boolean fromUser) {
                        if (mediaPlayer != null && fromUser) {
                            mediaPlayer.seekTo(progress);
                        }
                    }
                });


            }

            //repeat above code every second
            mHandler.postDelayed(this, 10);
        }
    };
    public AudioFragment() {
        // Required empty public constructor
    }

    // TODO: Rename and change types and number of parameters
    public static AudioFragment newInstance(String param1, String param2) {
        AudioFragment fragment = new AudioFragment();
        Bundle args = new Bundle();
        args.putString(ARG_PARAM1, param1);
        args.putString(ARG_PARAM2, param2);
        fragment.setArguments(args);
        return fragment;
    }

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        if (getArguments() != null) {
            mParam1 = getArguments().getString(ARG_PARAM1);
            mParam2 = getArguments().getString(ARG_PARAM2);
        }


        getActivity().invalidateOptionsMenu();

    }

    @Override
    public View onCreateView(final LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        //  Inflate the layout for this fragment
        final View v= inflater.inflate(R.layout.fragment_video_gallery, container, false);

        progress = (ProgressBar)v. findViewById(R.id.progress_bar);
        mSearchView = (FloatingSearchView) v.findViewById(R.id.floating_search_view);
        mLayout = (SlidingUpPanelLayout)  v.findViewById(R.id.sliding_layout);
        bottomplay=(RelativeLayout)  v.findViewById(R.id.bottomplay);
        now_playing_audio = (TextView)v. findViewById(R.id.now_playing_text);

        cntrl = (RelativeLayout)  v.findViewById(R.id.dragView);
        cntrl.setTag("cntrl");
        rv = (RecyclerView)v.findViewById(R.id.rv);
        expandableListView= (ExpandableListView) v.findViewById(R.id.expandableListView);
        implay= (ImageView)v.findViewById(R.id.play);
        //  impause= (ImageButton)v.findViewById(R.id.pause);
        imforward= (ImageView)v.findViewById(R.id.forward);
        downplay= (ImageView)v.findViewById(R.id.downplay);
        imbackward= (ImageView)v.findViewById(R.id.backward);
        seekBar = (SeekBar) v.findViewById(R.id.seekBar);
        totalTime = (TextView)v. findViewById(R.id.totalTime);
        mImageView = (ImageView)v. findViewById(R.id.coverImage);
        currentTime = (TextView)v. findViewById(R.id.currentTime);
        //   progress = (ProgressBar)v. findViewById(R.id.progress_bar);
        category= (TextView) v.findViewById(R.id.category);

        options= (ImageView) v.findViewById(R.id.img_menu);
        prepareListData();

        mediaPlayer = new MediaPlayer();
        mediaPlayer.setAudioStreamType(AudioManager.STREAM_MUSIC);
        final PopupWindow popupWindow = new PopupWindow(getActivity());


        NetworkCheckingClass networkCheckingClass1 = new NetworkCheckingClass(getActivity());
        boolean is = networkCheckingClass1.ckeckinternet();
        if (is)
        {  expandableListView.setVisibility(View.INVISIBLE);
            progress.setVisibility(View.VISIBLE);
//            progress.setVisibility(ProgressBar.VISIBLE);
                   StringRequest stringRequest = new StringRequest(Request.Method.POST, REGISTER_URL,
                new Response.Listener<String>() {
                    @Override
                    public void onResponse(String response) {
                        //  progress.setVisibility(ProgressBar.GONE);
                        System.out.println("responseeeee1"+response);
                        System.out.println("responseeeee1"+response); System.out.println("responseeeee1"+response);
                        System.out.println("responseeeee1"+response);


///////////////////////////////////////////////1111111111##############################999999999999999//////////////////////////////////////////////////////////////////////
                        System.out.println("responseeeee1"+response);
                        System.out.println("responseeeee1"+response); System.out.println("responseeeee1"+response);
                        System.out.println("responseeeee1"+response);
                        audioModelArrayList =new ArrayList<>();
                        AudioModel audioModel;

                        try {
                            System.out.println("bindhya..............."+response);
                            System.out.println("bindhya..............."+response);
                            JSONArray jsonArray=new JSONArray(response);
                            for(int i=0;i< jsonArray.length();i++)
                            {

                                JSONObject jsonobject =jsonArray.getJSONObject(i);
                                audioModel =new AudioModel();
                                String category_id = jsonobject.getString("category_id");
                                String sub_category_id = jsonobject.getString("sub_category_id");
                                audio = jsonobject.getString("audio");
                                thumbnail = jsonobject.getString("thumbnail");
                                String Orator = jsonobject.getString("Orator");
                                String  title = jsonobject.getString("title");
                                //  String Subject = jsonobject.getString("Subject");
                                //  String Programme = jsonobject.getString("Programme");
                                audioModel.setCategory_id(category_id);
                                System.out.println("xxx"+audio);
                                audioModel.setSub_category_id(sub_category_id);
                                audioModel.setAudio(audio);
                                audioModel.setOrator(Orator);
                                audioModel.setTitle(title);
//                                    audioModel.setProgramme(Programme);
//                                    audioModel.setSubject(Subject);
                                audioModel.setThumbnail(thumbnail);

                                audioModelArrayList.add(audioModel);



                            }


                            filteredlistclick= audioModelArrayList;
                            rvAdapterAudioList = new RVAdapterAudioList(filteredlistclick,getActivity());
                            System.out.println("xxx");
                            if (Build.VERSION.SDK_INT >= 23) {
                                if (isReadStorageAllowed()) {
                                    //If permission is already having then showing the toast
                                  /*  Toast.makeText(getActivity(), "You already have the permission", Toast.LENGTH_LONG).show();*/
                                    //Existing the method with return
                                    System.out.println("You already have the permission");
                                    rv.setAdapter(rvAdapterAudioList);

                                } else
                                //If the app has not the permission then asking for the permission
                                {
                                    System.out.println("You dont already have the permission");
                                    requestStoragePermission();
                                    rv.setAdapter(rvAdapterAudioList);
                                }
                            }
                            rv.setAdapter(rvAdapterAudioList);
                            System.out.println("xxx1");
                            progress.setVisibility(View.GONE);
                            rv.addOnItemTouchListener(
                                    new RecyclerItemClickListener(getActivity() ,new RecyclerItemClickListener.OnItemClickListener() {
                                        @Override public void onItemClick(View view, final int position) {
                                            synchronized (this) {
                                                expandableListView.setVisibility(View.INVISIBLE);


                                            }

                                        }


                                    })
                            );


                        } catch (JSONException e) {
                            e.printStackTrace();
                        }

                    }
                },
                new Response.ErrorListener() {
                    @Override
                    public void onErrorResponse(VolleyError error) {

                    }
                })

        {
            @Override
            protected Map<String, String> getParams() throws AuthFailureError {
                Map<String, String> params = new HashMap<String, String>();



                params.put("fid", "3");
                params.put("category_id", "1");
//                    params.put("subcategory_id", "1");



                return params;
            }

        };

        RequestQueue requestQueue = Volley.newRequestQueue(getActivity());
//            int socketTimeout = 30000;//30 seconds - change to what you want
//            RetryPolicy policy = new DefaultRetryPolicy(socketTimeout, DefaultRetryPolicy.DEFAULT_MAX_RETRIES, DefaultRetryPolicy.DEFAULT_BACKOFF_MULT);
//            stringRequest.setRetryPolicy(policy);
        requestQueue.add(stringRequest);


    }


   // com.meridian.voiceofislam.audioplayer.ExpandableListAdapter expandableListAdapter = new com.meridian.voiceofislam.audioplayer.ExpandableListAdapter(getActivity(), listDataHeader, listDataChild);

        expandableListAdapter = new ExpandableListAdapter(getActivity(), listDataHeader, listDataChild,listchild);
        // setting list rvAdapterAudioList
        expandableListView.setAdapter(expandableListAdapter);
        expandableListView.setOnGroupClickListener(new ExpandableListView.OnGroupClickListener() {

            @Override
            public boolean onGroupClick(ExpandableListView parent, View v,
                                        int groupPosition, long id) {
               /* Toast.makeText(getActivity(),
                        "Group Clicked " + listDataHeader.get(groupPosition),
                        Toast.LENGTH_SHORT).show();*/
                return false;
            }
        });

        expandableListView.setOnGroupExpandListener(new ExpandableListView.OnGroupExpandListener() {
            @Override
            public void onGroupExpand(final int groupPosition) {
                /*Toast.makeText(  getActivity(),
                        listDataHeader.get(groupPosition) + " Expanded",
                        Toast.LENGTH_SHORT).show();*/



            }
        });

        // Listview Group collasped listener
        expandableListView.setOnGroupCollapseListener(new ExpandableListView.OnGroupCollapseListener() {

            @Override
            public void onGroupCollapse(int groupPosition) {
                /*Toast.makeText(  getActivity(),
                        listDataHeader.get(groupPosition) + " Collapsed",
                        Toast.LENGTH_SHORT).show();*/

            }
        });

        // Listview on child click listener
        expandableListView.setOnChildClickListener(new ExpandableListView.OnChildClickListener() {

            @Override
            public boolean onChildClick(ExpandableListView parent, View v,
                                        final int groupPosition, final int childPosition, long id) {
                // TODO Auto-generated method stub



                expandableListView.setVisibility(View.INVISIBLE);
               /* Toast.makeText(
                        getActivity(),
                        listDataHeader.get(groupPosition)
                                + " : "
                                + listchild.get(
                                listDataHeader.get(groupPosition)).get(
                                childPosition)+"click", Toast.LENGTH_SHORT).show();*/

json();
                        return false;

            }

        });



        options.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                i++;
                if (i == 1) {
                    //Single click
                    expandableListView.setVisibility(View.VISIBLE);
                } else if (i == 2) {
                    //Double click

                    expandableListView.setVisibility(View.GONE);
                    i = 0;
                    //   Toast.makeText(getActivity(),"clicked",Toast.LENGTH_LONG).show();
                }
            }
        });




        imforward.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {

                int seekForwardTime = 5000;

                // get current song position
                int currentPosition = mediaPlayer.getCurrentPosition();
                // check if seekForward time is lesser than song duration
                if(currentPosition + seekForwardTime <= mediaPlayer.getDuration()){
                    // forward song
                    mediaPlayer.seekTo(currentPosition + seekForwardTime);
                }else{
                    // forward to end position
                    mediaPlayer.seekTo(mediaPlayer.getDuration());
                }

            }
        });
        imbackward.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                //set seek time
                int seekBackwardTime = 5000;

                // get current song position
                int currentPosition = mediaPlayer.getCurrentPosition();
                // check if seekBackward time is greater than 0 sec
                if(currentPosition - seekBackwardTime >= 0){
                    // forward song
                    mediaPlayer.seekTo(currentPosition - seekBackwardTime);
                }else{
                    // backward to starting position
                    mediaPlayer.seekTo(0);
                }

            }
        });
        downplay.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {


                if(mediaPlayer.isPlaying())
                {
                    implay.setBackgroundResource(R.drawable.newplay);
                    mediaPlayer.pause();
                    downplay.setBackgroundResource(R.drawable.ic_action_play);
//
//                    implay.setBackgroundColor(Color.TRANSPARENT);
//                    impause.setBackgroundColor(Color.parseColor("#80f7473e"));

                }
                else
                {   // progress.setVisibility(ProgressBar.VISIBLE);

                    implay.setBackgroundResource(R.drawable.new_pause);
                    mediaPlayer.start();
                    downplay.setBackgroundResource(R.drawable.ic_action_pause);
                    Intent serviceIntent = new Intent(getActivity(), NotificationService.class);
                    serviceIntent.setAction(Constants.ACTION.STARTFOREGROUND_ACTION);
                    getActivity().startService(serviceIntent);
                    System.out.println("clickd1");
                    //   progress.setVisibility(ProgressBar.INVISIBLE);
                }

            }
        });

        implay.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if(mediaPlayer.isPlaying())
                {        implay.setBackgroundResource(R.drawable.newplay);

                    mediaPlayer.pause();
                    downplay.setBackgroundResource(R.drawable.ic_action_play);


                }
                else
                {   implay.setBackgroundResource(R.drawable.new_pause);
                    mediaPlayer.start();
                    downplay.setBackgroundResource(R.drawable.ic_action_pause);
                    Intent serviceIntent = new Intent(getActivity(), NotificationService.class);
                    serviceIntent.setAction(Constants.ACTION.STARTFOREGROUND_ACTION);
                    getActivity().startService(serviceIntent);
                }

            }
        });
        mImageView = (ImageView) v.findViewById(R.id.coverImage);
        rv.setHasFixedSize(true);
        TextView currentTime = (TextView) v.findViewById(R.id.currentTime);

        LinearLayoutManager llm = new LinearLayoutManager(getActivity());
        llm.setOrientation(LinearLayoutManager.VERTICAL);
        rv.setLayoutManager(llm);

        Context con=getActivity();

        final String coverImage ="https://dl.dropboxusercontent.com/u/2763264/RSS%20MP3%20Player/img3.jpg";



        String image_url = coverImage;


        mediaPlayer.setOnPreparedListener(new MediaPlayer.OnPreparedListener() {
            public void onPrepared(final MediaPlayer mp)
            {
                if(mp.isPlaying()) {
                    implay.setBackgroundResource(R.drawable.newplay);

                    mediaPlayer.pause();
                    downplay.setBackgroundResource(R.drawable.ic_action_play);

                }
                else {
                    implay.setBackgroundResource(R.drawable.new_pause);
                    mediaPlayer.start();
                    downplay.setBackgroundResource(R.drawable.ic_action_pause);
                    System.out.println("clickd2");
                }

                mRunnable.run();

            }
        });


        mLayout.setFadeOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                //  Toast.makeText(getActivity(),"panelslide", Toast.LENGTH_SHORT).show();
                mLayout.setPanelState(SlidingUpPanelLayout.PanelState.COLLAPSED);
            }
        });
        mediaPlayer.setOnCompletionListener(new MediaPlayer.OnCompletionListener() {

            @Override
            public void onCompletion(MediaPlayer mp) {
                implay.setBackgroundResource(R.drawable.newplay);
                seekBar.setProgress(0);

                downplay.setBackgroundResource(R.drawable.ic_action_play);
            }

        });






        mSearchView.setOnQueryChangeListener(new FloatingSearchView.OnQueryChangeListener()
        {
            @Override
            public void onSearchTextChanged(String oldQuery, final String newQuery) {

                // Toast.makeText(getActivity(), ""+newQuery+","+oldQuery,Toast.LENGTH_SHORT).show();


                mSearchView.showProgress();

                rv.scrollToPosition(0);

                mSearchView.hideProgress();
                if(rvAdapterAudioList !=null) {
                    rvAdapterAudioList.filter(newQuery);
                }
                 if(rvAdapterFilterAudio !=null) {
                    rvAdapterFilterAudio.filter(newQuery);
                }

              if(rvAdapterFilterAudioContent !=null) {
                   rvAdapterFilterAudioContent.filter(newQuery);
                }

            }

            //  }
        });


        setHasOptionsMenu(true);



        return v;


    }

    // TODO: Rename method, update argument and hook method into UI event
    public void onButtonPressed(Uri uri) {
        if (mListener != null) {
            mListener.onFragmentInteraction(uri);
        }
    }

    @Override
    public void onAttach(Context context) {
        super.onAttach(context);
        if (context instanceof OnFragmentInteractionListener) {
            mListener = (OnFragmentInteractionListener) context;
        } else {
            throw new RuntimeException(context.toString()
                    + " must implement OnFragmentInteractionListener");
        }
    }

    @Override
    public void onDetach() {
        super.onDetach();
        mListener = null;
    }

    @Override
    public void onClick(View view) {
       // switch()

    }

    public void stop(View view){

        mediaPlayer.seekTo(0);
        mediaPlayer.pause();

    }

    private String getTimeString(long millis) {
        StringBuffer buf = new StringBuffer();

        long hours = millis / (1000*60*60);
        long minutes = ( millis % (1000*60*60) ) / (1000*60);
        long seconds = ( ( millis % (1000*60*60) ) % (1000*60) ) / 1000;

        buf
                .append(String.format("%02d", hours))
                .append(":")
                .append(String.format("%02d", minutes))
                .append(":")
                .append(String.format("%02d", seconds));

        return buf.toString();
    }

    @Override
    public void onDestroy() {

        super.onDestroy();
        Intent serviceIntent = new Intent(getActivity(), NotificationService.class);
        serviceIntent.setAction(Constants.ACTION.STOPFOREGROUND_ACTION);
        getActivity().startService(serviceIntent);
    }

    public boolean isReadStorageAllowed() {
        //Getting the permission status
        int result = ContextCompat.checkSelfPermission(getActivity(), Manifest.permission.WRITE_EXTERNAL_STORAGE);

        //If permission is granted returning true
        if (result == PackageManager.PERMISSION_GRANTED)
            return true;

        //If permission is not granted returning false
        return false;
    }

    //Requesting permission
    private void requestStoragePermission(){

        if (ActivityCompat.shouldShowRequestPermissionRationale(getActivity(),Manifest.permission.WRITE_EXTERNAL_STORAGE)){
            //If the user has denied the permission previously your code will come to this block
            //Here you can explain why you need this permission
            //Explain here why you need this permission
        }

        //And finally ask for the permission
        ActivityCompat.requestPermissions(getActivity(),new String[]{Manifest.permission.WRITE_EXTERNAL_STORAGE},1);
    }

    //This method will be called when the user will tap on allow or deny
    @Override
    public void onRequestPermissionsResult(int requestCode, @NonNull String[] permissions, @NonNull int[] grantResults) {

        //Checking the request code of our request
        if(requestCode == 1){

            //If permission is granted
            if(grantResults.length >0 && grantResults[0] == PackageManager.PERMISSION_GRANTED){
                System.out.println("Permission granted now you can read the storage");
                //Displaying a toast
                Toast.makeText(getActivity(),"Permission granted now you can read the storage",Toast.LENGTH_LONG).show();
            }else{
                //Displaying another toast if permission is not granted
                System.out.println("Oops you just denied the permission");
                Toast.makeText(getActivity(),"Oops you just denied the permission",Toast.LENGTH_LONG).show();
            }
        }
    }

    public  void prepareListData() {
        listDataHeader = new ArrayList<String>();
        listDataChild = new HashMap<String, List<String>>();
        listchild=new HashMap<String,List<String>>();

        listDataHeader.add("Speeches");
        listDataHeader.add("Quthuba");
        listDataHeader.add("Voice Of Islam");
        listDataHeader.add("Islamic Songs");
        listDataHeader.add("Quran Recitation");

        List<String> Speeches = new ArrayList<String>();
        Speeches .add("Filter by Orator");
        Speeches .add("Filter by Subject");
        Speeches .add("Filter by Programme");



        List<String> Quthuba = new ArrayList<String>();
        Quthuba.add("New");
        Quthuba.add("Filter by Katheeb");
        Quthuba.add("Filter by Masjid");

        List<String> VoiceOfslam = new ArrayList<String>();
        VoiceOfslam.add("Thought for the day");
        VoiceOfslam.add("Short audio clips");
        VoiceOfslam.add("Organizational response in recent issues");


        listDataChild.put(listDataHeader.get(0),Speeches); // Header, Child data
        listDataChild.put(listDataHeader.get(1), Quthuba);
        listDataChild.put(listDataHeader.get(2), VoiceOfslam);
        listDataChild.put(listDataHeader.get(3), VoiceOfslam);
        listDataChild.put(listDataHeader.get(4), VoiceOfslam);


        List<String>Orator=new ArrayList<String>();
        Orator.add("Filter by date");
        Orator.add("Filter by views");

       // listchild.get(listDataHeader.get(0)).get(0);
        listchild.put(listDataHeader.get(0),Orator);
        listchild.put(listDataHeader.get(1),Orator);





    }

    private Intent getShareIntent(String type, String subject, String text)
    {
        boolean found = false;
        Intent share = new Intent(Intent.ACTION_SEND);
        share.setType("text/plain");

        // gets the list of intents that can be loaded.
        List<ResolveInfo> resInfo = getActivity().getPackageManager().queryIntentActivities(share, 0);
        System.out.println("resinfo: " + resInfo);
        if (!resInfo.isEmpty()){
            for (ResolveInfo info : resInfo) {
                if (info.activityInfo.packageName.toLowerCase().contains(type) ||
                        info.activityInfo.name.toLowerCase().contains(type) ) {
                    share.putExtra(Intent.EXTRA_SUBJECT,  subject);
                    share.putExtra(Intent.EXTRA_TEXT,     text);
                    share.setPackage(info.activityInfo.packageName);
                    found = true;
                    break;
                }
            }
            if (!found)
                return null;

            return share;
        }
        return null;
    }

    private void json() {
        RequestQueue queue3 = Volley.newRequestQueue(getActivity());
        String url3 = "http://app.knm.com.php56-9.dfw3-2.websitetestlink.com/services/response.php?fid=12&cat_id=1&subcat_id=1&filter_id=1&filter_status=filter";
progress.setVisibility(View.VISIBLE);
        StringRequest stringRequest3 = new StringRequest(Request.Method.GET, url3, new Response.Listener<String>() {
            @Override
            public void onResponse(String response) {
                // Display the first 500 characters of the response string.
                //  tv.setText("Response is: "+ response);
//pd.dismiss();
                System.out.println("++++++++++++++RESPONSE+++++++++++++++ gallery :" + response);
                if (response != null) {

                    JSONArray jsonarray;
                    JSONObject jsonobject;
                    //  LibraryModel lb;
                    try {
                        arrayList=new ArrayList<>();
                        jsonarray = new JSONArray(response);
                        System.out.println("g2");
                        for (int i = 0; i < jsonarray.length(); i++) {
                            AudioFilterModel audioFilterModel =new AudioFilterModel();
                       AudioModel audioModel =new AudioModel();

                            // lb = new LibraryModel();
                            System.out.println("g3");
                            jsonobject = jsonarray.getJSONObject(i);
                            String filter_content_id = jsonobject.getString("filter_content_id");
                            String filter_content = jsonobject.getString("filter_content");

                            audioFilterModel.setFilter_content_id(filter_content_id);
                            audioFilterModel.setFilter_content(filter_content);
                            audioModel.setFilter_content(filter_content);



                            //      lb.setPdf(pdf);
                            //      lb.setThmb(thumbnail);

                            // gm2.setGalimag(url2);
                            arrayList.add(audioFilterModel);
                            audioModelArrayList.add(audioModel);

                        }
                               /* ListAdapter expandableListAdapter =new ListAdapter(NewActivity.this,arrayList);
                                listView.setAdapter(expandableListAdapter);
*/

                        rvAdapterFilterAudio = new RVAdapterFilterAudio(arrayList,getActivity());
                        System.out.println("xxx");
progress.setVisibility(View.GONE);
                        rv.setAdapter(rvAdapterFilterAudio);
                        System.out.println("xxx1");
                        rv.addOnItemTouchListener(
                                new RecyclerItemClickListener(getActivity() ,new RecyclerItemClickListener.OnItemClickListener() {
                                    @Override public void onItemClick(View view, final int position) {
                                        synchronized (this) {
                                            expandableListView.setVisibility(View.INVISIBLE);
                                            final String s=arrayList.get(position).getFilter_content_id();
                                            System.out.println("String...............++++++++++" + s);

                                            NetworkCheckingClass networkCheckingClass1 = new NetworkCheckingClass(getActivity());
                                            boolean is = networkCheckingClass1.ckeckinternet();
                                            if (is) {
                                                progress.setVisibility(View.VISIBLE);
                                                expandableListView.setVisibility(View.INVISIBLE);
                                             String   REGISTER_URLs="http://app.knm.com.php56-9.dfw3-2.websitetestlink.com/services/response.php";
//            progress.setVisibility(ProgressBar.VISIBLE);
                                                StringRequest stringRequest0 = new StringRequest(Request.Method.POST, REGISTER_URLs,
                                                        new Response.Listener<String>() {
                                                            @Override
                                                            public void onResponse(String response) {
                                                                subCategoryModelArrayList2=new ArrayList<>();
                                                          AudioFilterContentModel audioFilterContentModel;
                                                                String title=null;

                                                                try {

                                                                    JSONArray jsonArray=new JSONArray(response);
                                                                    for(int i=0;i< jsonArray.length();i++)
                                                                    {

                                                                        JSONObject jsonobject =jsonArray.getJSONObject(i);
                                                                        audioFilterContentModel =new AudioFilterContentModel();

                                                                        String category_id = jsonobject.getString("category_id");
                                                                        String sub_category_id = jsonobject.getString("sub_category_id");
                                                                     String   audio = jsonobject.getString("audio");
                                                                    String    thumbnail = jsonobject.getString("thumbnail");
                                                                        String id = jsonobject.getString("id");
                                                                        if(jsonobject.has("title")) {
                                                                          title = jsonobject.getString("title");
                                                                        }
                                                                        else {
                                                                          title=null;
                                                                        }
                                                                            //  String Programme = jsonobject.getString("Programme");
                                                                            audioFilterContentModel.setCategory_id(category_id);
                                                                            System.out.println("xxxid" + id+category_id+sub_category_id+audio+thumbnail+title);
                                                                            audioFilterContentModel.setSub_category_id(sub_category_id);
                                                                            audioFilterContentModel.setAudio(audio);

                                                                            audioFilterContentModel.setTitle(title);

                                                                            audioFilterContentModel.setThumbnail(thumbnail);

                                                                           subCategoryModelArrayList2.add(audioFilterContentModel);


                                                                        }
                                                               progress.setVisibility(View.GONE);
                                                               rvAdapterFilterAudioContent =new RVAdapterFilterAudioContent(subCategoryModelArrayList2,getActivity());
                                                                        System.out.println("xxxsss");
                                                                        if (Build.VERSION.SDK_INT >= 23) {
                                                                            if (isReadStorageAllowed()) {
                                                                                //If permission is already having then showing the toast
                                                                                /*Toast.makeText(getActivity(), "You already have the permission", Toast.LENGTH_LONG).show();*/
                                                                                //Existing the method with return
                                                                                System.out.println("You already have the permission");
                                                                                //  listView.setAdapter(rvAdapterAudioList);
                                                                                rv.setAdapter(rvAdapterFilterAudioContent);

                                                                            } else
                                                                            //If the app has not the permission then asking for the permission
                                                                            {
                                                                                System.out.println("You dont already have the permission");
                                                                                requestStoragePermission();
                                                                                rv.setAdapter(rvAdapterFilterAudioContent);
                                                                            }
                                                                        }
                                                                        rv.setAdapter(rvAdapterFilterAudioContent);
                                                                        System.out.println("xxx1");
                                                                        rv.addOnItemTouchListener(
                                                                                new RecyclerItemClickListener(getActivity(), new RecyclerItemClickListener.OnItemClickListener() {
                                                                                    @Override
                                                                                    public void onItemClick(View view, final int position) {
                                                                                        synchronized (this) {
                                                                                            expandableListView.setVisibility(View.INVISIBLE);


                                                                                        }

                                                                                    }


                                                                                })
                                                                        );


                                                                    }
                                                                catch (JSONException e) {
                                                                    e.printStackTrace();
                                                                }

                                                            }
                                                        },
                                                        new Response.ErrorListener() {
                                                            @Override
                                                            public void onErrorResponse(VolleyError error) {

                                                            }
                                                        })

                                                {
                                                    @Override
                                                    protected Map<String, String> getParams() throws AuthFailureError {
                                                        Map<String, String> params = new HashMap<String, String>();


                                                        params.put("fid", "13");
                                                        params.put("filter_content_id", s);
//                    params.put("subcategory_id", "1");


                                                        return params;
                                                    }

                                                };

                                                RequestQueue requestQueue0 = Volley.newRequestQueue(getActivity());
//            int socketTimeout = 30000;//30 seconds - change to what you want
//            RetryPolicy policy = new DefaultRetryPolicy(socketTimeout, DefaultRetryPolicy.DEFAULT_MAX_RETRIES, DefaultRetryPolicy.DEFAULT_BACKOFF_MULT);
//            stringRequest.setRetryPolicy(policy);
                                                requestQueue0.add(stringRequest0);


                                            }



                                        }

                                    }


                                })
                        );


                        // mRecyclerView.setAdapter(adapter0);







                    } catch (JSONException e) {
                        e.printStackTrace();
                    }
                } else {
                    System.out.println("nothing to displayy");
                }
            }


        }, new Response.ErrorListener() {
            @Override
            public void onErrorResponse(VolleyError error) {
                //tv.setText("That didn't work!");

            }
        });

        queue3.add(stringRequest3);
    }

    public interface OnFragmentInteractionListener {
        // TODO: Update argument type and name
        void onFragmentInteraction(Uri uri);
    }
}


