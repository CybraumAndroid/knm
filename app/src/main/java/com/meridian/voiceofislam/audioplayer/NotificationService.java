package com.meridian.voiceofislam.audioplayer;

import android.annotation.TargetApi;
import android.app.Notification;
import android.app.PendingIntent;
import android.os.Build;
import android.os.IBinder;
import android.app.Service;
import android.content.Intent;
import android.view.View;
import android.widget.RemoteViews;


import com.meridian.voiceofislam.MainActivity;
import com.meridian.voiceofislam.R;

public class NotificationService extends Service {
    Notification status;
    private final String LOG_TAG = "NotificationService";
    @Override
    public void onDestroy() {
        super.onDestroy();
    }

    @Override
    public IBinder onBind(Intent intent) {
        return null;
    }

    @Override
    public int onStartCommand(Intent intent, int flags, int startId) {
        if (null == intent || null == intent.getAction ()) {
            stopSelf();
            String source = null == intent ? "intent" : "action";
            //   Log.e (TAG, source + " was null, flags=" + flags + " bits=" + Integer.toBinaryString (flags));
            return START_STICKY;
        }
        if (intent.getAction().equals(Constants.ACTION.STARTFOREGROUND_ACTION)) {
            showNotification();

            //  Toast.makeText(this, "Service Started", Toast.LENGTH_SHORT).show();

        }
        else if (intent.getAction().equals(Constants.ACTION.PAUSE_ACTION))
        {    AudioFragment.implay.setBackgroundResource(R.drawable.newplay);
            AudioFragment.mediaPlayer.pause();
            //    ListMp3.downplay.setBackgroundResource(R.drawable.ic_action_play);
            // ListMp3.implay.setBackgroundResource(R.drawable.newplay);
            AudioFragment. downplay.setBackgroundResource(R.drawable.ic_action_play);
            //  AudioFragment.downplay.performClick();
            //   Toast.makeText(this, "clicked pause", Toast.LENGTH_SHORT).show();

        }
        else if (intent.getAction().equals(Constants.ACTION.PREV_ACTION)) {


        } else if (intent.getAction().equals(Constants.ACTION.PLAY_ACTION)) {
            //   Toast.makeText(this, "Clicked Play", Toast.LENGTH_SHORT).show();
            System.out.println("clickd");
            AudioFragment.downplay.performClick();

        } else if (intent.getAction().equals(Constants.ACTION.NEXT_ACTION)) {

        } else if (intent.getAction().equals(
                Constants.ACTION.STOPFOREGROUND_ACTION)) {

            //   Toast.makeText(this, "Service Stoped", Toast.LENGTH_SHORT).show();
            stopService(intent);
            stopForeground(true);
            AudioFragment.mediaPlayer.stop();
           // ListMp3.mediaPlayer.stop();
            //   AudioFragment.bottomplay.setVisibility(View.INVISIBLE);
            stopSelf();

        }
        return START_STICKY;
    }


    @TargetApi(Build.VERSION_CODES.JELLY_BEAN)
    private void showNotification() {
// Using RemoteViews to bind custom layouts into Notification
        RemoteViews views = new RemoteViews(getPackageName(),
                R.layout.status_bar);
//        RemoteViews bigViews = new RemoteViews(getPackageName(),
//                R.layout.status_bar_expanded);

// showing default album image
        views.setViewVisibility(R.id.status_bar_icon, View.VISIBLE);
        //views.setViewVisibility(R.id.status_bar_album_art, View.GONE);
//        bigViews.setImageViewBitmap(R.id.status_bar_album_art,
//                Constants.getDefaultAlbumArt(this));

        Intent notificationIntent = new Intent(this, MainActivity.class);
        notificationIntent.setAction(Constants.ACTION.MAIN_ACTION);
        notificationIntent.setFlags(Intent.FLAG_ACTIVITY_NEW_TASK
                | Intent.FLAG_ACTIVITY_CLEAR_TASK);
        PendingIntent pendingIntent = PendingIntent.getActivity(this, 0,
                notificationIntent, 0);

        Intent previousIntent = new Intent(this, NotificationService.class);
        previousIntent.setAction(Constants.ACTION.PREV_ACTION);
        PendingIntent ppreviousIntent = PendingIntent.getService(this, 0,
                previousIntent, 0);

        Intent playIntent = new Intent(this, NotificationService.class);
        playIntent.setAction(Constants.ACTION.PLAY_ACTION);
        PendingIntent  pplayIntent = PendingIntent.getService(this, 0,
                playIntent, 0);

        Intent pauseIntent = new Intent(this, NotificationService.class);
        pauseIntent.setAction(Constants.ACTION.PAUSE_ACTION);
        PendingIntent ppauseIntent = PendingIntent.getService(this, 0,
                pauseIntent, 0);

        Intent nextIntent = new Intent(this, NotificationService.class);
        nextIntent.setAction(Constants.ACTION.NEXT_ACTION);
        PendingIntent pnextIntent = PendingIntent.getService(this, 0,
                nextIntent, 0);

        Intent closeIntent = new Intent(this, NotificationService.class);
        closeIntent.setAction(Constants.ACTION.STOPFOREGROUND_ACTION);
        PendingIntent pcloseIntent = PendingIntent.getService(this, 0,
                closeIntent, 0);

        views.setOnClickPendingIntent(R.id.status_bar_play, pplayIntent);


        views.setOnClickPendingIntent(R.id.status_bar_pause,ppauseIntent);
        //views.setOnClickPendingIntent(R.id.status_bar_collapse, pcloseIntent);

        views.setImageViewResource(R.id.status_bar_play,
                R.drawable.ic_action_play);


        status = new Notification.Builder(this).build();
        status.contentView = views;
        status.flags = Notification.FLAG_ONGOING_EVENT;
        status.icon = R.mipmap.ic_launcher;
        status.contentIntent = pendingIntent;



        startForeground(Constants.NOTIFICATION_ID.FOREGROUND_SERVICE, status);

    }

}